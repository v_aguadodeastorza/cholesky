#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
//#include <omp.h>


void chol_escalar(double *A, int n);

int main (int argc, char *argv[]) {
	
	if (argc<2) {
		fprintf(stderr, "usage: %s n\n", argv[0]);
		exit(-1);
	}
	
	int n = atoi(argv[1]);
	double *A;
	double *B;
	double *S;
	
	A = (double*)malloc(n*n*sizeof(double));
	B = (double*)malloc(n*n*sizeof(double));
	S = (double*)malloc(n*n*sizeof(double));
	
	int i, j, k;
	//printf("Matriz A:\n");
	for (i=0;i<n;i++) {
		for (j=0;j<n;j++) {
			if (i>=j)
			A[i*n+j]=(double)rand()/(double)RAND_MAX*10;
			//printf("%f ", A[i*n+j]);
		}
		//printf("\n");
	}
	//printf("\n");
	
	//printf("Matriz transpuesta de A:\n");
	
	
	for (i=0;i<n;i++) {
			for (j=0;j<n; j++) {
				B[i*n+j]=A[j*n+i];
				//printf("%f ", B[i*n+j]);
			}
		//printf("\n");
	}
	
	//printf("\n");
	
	//printf("Multiplicación de A por transpuesta de A:\n");
	
//#pragma omp parallel for private(j,k)
	for (i=0;i<n;i++) {
			for (j=0;j<n; j++) {
				S[i*n+j]=0;
				for (k=0;k<n;k++) {
					S[i*n+j]=(S[i*n+j]+(A[i*n+k]*B[k*n+j]));
				}
				//printf("%f ", S[i*n+j]);
			}
			//printf("\n");
		}
		//printf("\n");
	
	chol_escalar(S, n);
	free(A);
	free(B);
	free(S);
	return 0;
	
}
	
void chol_escalar(double *A, int n) {
	
	double *C;
	int i, j, k;
	double c;
	
	C = (double*)malloc(n*n*sizeof(double));
	
	//printf("Matriz C antes:\n");
	
	for (i=0;i<n;i++) {
		for (j=0;j<n;j++) {
			C[i*n+j]=A[i*n+j];
			//printf("%f ", C[i*n+j]);
		}
		//printf("\n");
	}
	//printf("\n");
	clock_t start = clock();
	
	for (k=0;k<n;k++) {
		c = sqrt(C[k*n+k]);
		C[k*n+k]=c;
		for (i=k+1;i<n;i++) {
			C[i*n+k]=C[i*n+k]/c;
		}
		for (i=k+1;i<n;i++) {
			for (j=k+1;j<=i-1;j++) {
				C[i*n+j] = C[i*n+j] - C[i*n+k]*C[j*n+k];
			}
			C[i*n+i] = C[i*n+i] - C[i*n+k]*C[i*n+k];
		}
	}
	
	printf("Tiempo algoritmo: %f\n", ((double)clock() - start)/CLOCKS_PER_SEC);
	
/*	printf("Matriz C después:\n");
	
		for (i=0;i<n;i++) {
			for (j=0;j<n;j++) {
				printf("%f ", C[i*n+j]);
			}
			printf("\n");
		}*/
	
	free(C);
}
