#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#if defined __INTEL_COMPILER
#include <mkl.h>
#elif defined __GNUC__
#include <clapack.h>
#endif

double blockCholesky(double* A,int n, int b);

int main (int argc, char* argv[]) {
	int n = atoi(argv[1]);
	int bloque = atoi(argv[2]);
	double *A;
	double *B;
	double *S;
	
	A = (double*)malloc(n*n*sizeof(double));
	B = (double*)malloc(n*n*sizeof(double));
	S = (double*)malloc(n*n*sizeof(double));
	
	int i, j, k;
	
	//printf("Matriz A:\n");
	for (i=0;i<n;i++) {
		for (j=0;j<n;j++) {
			if (i>=j)
			A[i*n+j]=(double)rand()/(double)RAND_MAX*10;
			//printf("%f ", A[i*n+j]);
		}
		//printf("\n");
	}
	//printf("\n");
	
	//printf("Matriz transpuesta de A:\n");
	
	for (i=0;i<n;i++) {
			for (j=0;j<n; j++) {
				B[i*n+j]=A[j*n+i];
				//printf("%f ", B[i*n+j]);
			}
		//printf("\n");
	}
	//printf("\n");
	
	//printf("Multiplicación de A por transpuesta de A:\n");

#pragma omp parallel for private(j,k)	
	for (i=0;i<n;i++) {
			for (j=0;j<n; j++) {
				S[i*n+j]=0;
				for (k=0;k<n;k++) {
					S[i*n+j]=(S[i*n+j]+(A[i*n+k]*B[k*n+j]));
				}
				//printf("%f ", S[i*n+j]);
			}
			//printf("\n");
		}
		//printf("\n");
	
#pragma omp parallel
#pragma omp single	
	printf("Tiempo de algoritmo con talla %d y bloque %d: %f\n", n, bloque, blockCholesky(S, n, bloque));
	free(A);
	free(B);
	free(S);
	
}

int min (int a, int b) {
if (a < b) return a;
return b;
}

double blockCholesky(double* A, int n, int b) {
	int k, mk,i,mi, j, mj;
	double ini,fin;
	double *C;
	C = (double*)malloc(n*n*sizeof(double));
	ini = dsecnd();
	cblas_dcopy(n*n,A,1,C,1);
	for(k = 0; k < n; k+=b) {
		mk = min(k+b,n);
		#pragma omp task
		LAPACKE_dpotrf(LAPACK_COL_MAJOR,'L',mk-k,&C[k+k*n],n);
		#pragma omp taskwait
		for(i = k+b; i < n; i+=b) {
			mi = min(i+b,n);
			#pragma omp task
			cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,
			CblasTrans,CblasNonUnit,mi-i,mk-k,
			1,&C[k+k*n],n,&C[i+k*n],n);
			#pragma omp taskwait
		}
		for(i = k+b; i < n; i+=b) {
			mi = min(i+b,n);
			for(j = k + b; j < (i-1); j+=b) {
				mj = min(j+b,n);
				#pragma omp task
				cblas_dgemm(CblasColMajor,CblasNoTrans,
				CblasTrans,mi-i,mj-j,mk-k,-1,&C[i+k*n],n
				,&C[j+k*n],n,1,&C[i+j*n],n);
			}
			#pragma omp task
			cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,
			mi-i,mk-k,-1,&C[i+k*n],n,1,&C[i+i*n],n);
			#pragma omp taskwait
		}
	}
	/*printf("Matriz C después:\n");
	
		for (i=0;i<n;i++) {
			for (j=0;j<n;j++) {
				printf("%f ", C[i+j*n]);
			}
			printf("\n");
		}*/
	fin = dsecnd();
	free(C);
	return fin-ini;
}
