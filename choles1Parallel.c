#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

#define C(i,j,n)  C[(i)+(j)*n]
#define	A(i,j,n)	A[j*n+i]


double* escalar( double *A, double *C, int n);
double* paralelo(double *A, double *C, int n);
void printMatrix (double *N, int n);
double t_inicial, t_final;
void main(int argc, char *argv[])
{
	int n= atoi(argv[1]);
	
	int j,i;
	double  *C;

	
	double *A =(double*)calloc(n*n, sizeof(double));
	for(j=0; j<n; j++){
		for(i=j; i<n; i++){
			A(i,j,n)=((double)rand())/RAND_MAX;

		}
	}
	for (j=0; j<n ; j++){
		A(j,j,n)+=1000;
	}
	
	

	//if((A=(double*)calloc(n*n,sizeof(double)))==NULL){
	//	fprintf(stderr,"Error reservando memoria de la matriz\n");
	//	exit(-1);
	//}
	//for(j = 0; j<n ; j++){
	//	for(i = j; i<n; i++){
	//		A[j*n+i]=((double)rand())/RAND_MAX;
	//		if(i==j)
	//		{
	//			A[j*n+i]= A[j*n+i]+1000;
	//		}
	//	}
		//A[j*n+j]+=n;
	//}
	
	
	

	//ejecución secuencial

	escalar(A,C,n);
	//printMatrix(C,n);

	//ejecución paralela

	paralelo(A,C,n);
	
	
	free(A);
	
	
}

// Escalar
double* escalar(double *A, double *C, int n ) {
    //omp_set_num_threads(1);
    
    int i, j, k;
    double c;
    if((C=(double*)calloc(n*n,sizeof(double)))==NULL){
		fprintf(stderr,"Error reservando memoria de la matriz\n");
		exit(-1);
	}
	int size=n*n;
	for(i=0; i<size; i++){
		C[i]=A[i];
	}

	printf("Descomposición de Cholesky ESCALAR de la matriz A(%dx%d):\n",n, n);   
	t_inicial = omp_get_wtime();
    for( k=0; k<n; k++ ){
        c = sqrt(C(k,k,n));
        C(k,k,n) = c;
	
        for( i=k+1;i<n; i++ )
            C(i,k,n)=C(i,k,n)/c;
	
        for(i=k+1;i<n;i++){
            for(j=k+1;j<=i-1;j++)
                C(i,j,n) = C(i,j,n) - C(i,k,n) * C(j,k,n);
            C(i,i,n) = C(i,i,n) - C(i,k,n) * C(i,k,n);
        }
    }
    t_final = omp_get_wtime();
    printf("El tiempo transcurrido en ESCALAR ha sido de: %lf\n",t_final-t_inicial);
	//printf("Ejecutado con %d hilos\n",  omp_get_max_threads());
    return C;
    
}

// PARALELO
double* paralelo( double *A, double *C, int n ) {
    int i, j, k;
    double c;
    if((C=(double*)calloc(n*n,sizeof(double)))==NULL){
		fprintf(stderr,"Error reservando memoria de la matriz\n");
		exit(-1);
	}
	int size=n*n;
	for(i=0; i<size; i++){
		C[i]=A[i];
	}
    
	printf("Descomposición de Cholesky PARALELO de la matriz A(%dx%d):\n",n, n); 
	t_inicial = omp_get_wtime();
   // omp_set_num_threads(4);
    #pragma omp parallel
    for( k=0; k<n; k++ ){
        c = sqrt(C(k,k,n));
        C(k,k,n) = c;
	#pragma omp for
        for( i=k+1;i<n; i++ )
            C(i,k,n)=C(i,k,n)/c;
	#pragma omp for private (j)
        for(i=k+1;i<n;i++){
            for(j=k+1;j<=i-1;j++)
                C(i,j,n) = C(i,j,n) - C(i,k,n) * C(j,k,n);
            C(i,i,n) = C(i,i,n) - C(i,k,n) * C(i,k,n);
        }
    }
    t_final = omp_get_wtime();

	printf("El tiempo transcurrido en PARALELO ha sido de: %lf\n",t_final-t_inicial);
	printf("Ejecutado con %d hilos\n",  omp_get_max_threads());
    return C;
    
	
}

void printMatrix (double *N, int n)
{
	int i, j;
	for (i=0; i<n; i++){
		for (j=0; j<n; j++){
			printf ("%f", N[i*n+j]);
		}
	printf("\n");	
	}
}


